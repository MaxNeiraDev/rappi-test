package test.cl.rappi_test.ui.fragments

import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import test.cl.rappi_test.R
import test.cl.rappi_test.api.models.Movie
import test.cl.rappi_test.databinding.FragmentListBinding
import test.cl.rappi_test.ui.adapters.MoviesListAdapter
import test.cl.rappi_test.utils.LoadingDialogManager
import test.cl.rappi_test.utils.Utility
import test.cl.rappi_test.viewmodels.ListFragmentViewModel
import test.cl.rappi_test.viewmodels.factories.ListFragmentViewModelFactory
import test.cl.rappi_test.viewmodels.MovieDetailsViewModel

class ListFragment: Fragment(), MoviesListAdapter.OnItemClickListener {

    private var myView: View? = null
    private var myContext: Context? = null
    private var binding: FragmentListBinding ?= null

    private lateinit var moviesList: MutableList<Movie>
    private lateinit var moviesListAdapter: MoviesListAdapter

    private val viewModel: ListFragmentViewModel by lazy { ViewModelProvider(this, ListFragmentViewModelFactory(myContext!!)).get(ListFragmentViewModel::class.java) }
    private val sharedViewModel: MovieDetailsViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)
        myContext = container!!.context
        binding = FragmentListBinding.inflate(inflater, container, false)
        myView = binding!!.root
        return myView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            lifecycleOwner = viewLifecycleOwner
            myViewModel = viewModel
            fragment = this@ListFragment
        }
        subscribeToModel()
        setupView()
        setupList()
        setupOnClickListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    override fun onItemClick(item: Movie, position: Int, view: View, holder: MoviesListAdapter.MyViewHolder) {
        if(Utility.isNetworkAvailable(requireActivity())) {
            Log.e("Item info", "Title: ${item.title} - Release date: ${item.releaseDate}")
            sharedViewModel.setClickedIndex(position)
            sharedViewModel.setMovie(item)
            showMovieDetailsView()
        }else{
            viewModel.showMessage("Click not available without connection", requireActivity())
        }
    }

    private fun subscribeToModel() {
        viewModel.getMoviesData().observe(requireActivity(), { t ->
            moviesList.clear()
            t?.let {
                moviesList.addAll(it)
                if(viewModel.moviesRepository.lastRequiredPage.value!!>1) binding!!.recyclerList.layoutManager!!.scrollToPosition(moviesListAdapter.lastShowedIndex+1)
                LoadingDialogManager.hideLoader()
                viewModel.saveCurrentElements(requireActivity())
            }
            moviesListAdapter.notifyDataSetChanged()
        })

        viewModel.getDBElements()?.observe(requireActivity(), {
            if(viewModel.getDataFromDBState()) {
                it?.let {
                    viewModel.useDataFromDB(it)
                }
            }
        })

        viewModel.topRatedTypeSelected.observe(requireActivity(), {
            if(it){
                setTypeSelected(binding!!.topRatedCategoryButton, binding!!.popularCategoryButton)
            }else{
                setTypeSelected(binding!!.popularCategoryButton, binding!!.topRatedCategoryButton)
            }
        })

        viewModel.changeMultimediaTypeAction.observe(requireActivity(), {
            if(it) viewModel.multimediasTypeChange(requireActivity())
        })

    }

    private fun setupOnClickListeners(){
        binding!!.topRatedCategoryButton.setOnClickListener {
                viewModel.topRatedTypeSelected.postValue(true)
                viewModel.changeMultimediaTypeAction.postValue(true)
        }

        binding!!.popularCategoryButton.setOnClickListener {
                viewModel.topRatedTypeSelected.postValue(false)
                viewModel.changeMultimediaTypeAction.postValue(true)
        }

        binding!!.searchViewButtonContainer.setOnClickListener { searchButtonClickAction() }
        binding!!.searchViewButton.setOnClickListener { searchButtonClickAction() }
    }

    private fun searchButtonClickAction(){
        if (Utility.isNetworkAvailable(requireActivity()))
            showSearchView()
        else
            viewModel.showMessage("Section not available without connection", requireActivity())
    }

    private fun setupView(){
        if(!Utility.isNetworkAvailable(requireActivity())) binding!!.multimediaCategoriesContainer.visibility = View.GONE
    }

    private fun setupList(){
        binding!!.recyclerList.layoutManager = LinearLayoutManager(myContext!!)
        moviesList = mutableListOf()
        viewModel.getMoviesData().value?.let { moviesList.addAll(it) }
        moviesListAdapter = MoviesListAdapter(myContext!!, moviesList,this)
        binding!!.recyclerList.adapter = moviesListAdapter

        binding!!.recyclerList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if(Utility.isNetworkAvailable(myContext!!) && viewModel.getDataFromWSState()) {
                        viewModel.getMovies(requireActivity())
                        moviesListAdapter.lastShowedIndex = moviesListAdapter.itemCount
                        LoadingDialogManager.showLoader(requireActivity(), "Loading new elements")
                    }
                }
            }
        })
    }

    private fun setTypeSelected(selectedButton: AppCompatButton, notSelectedButton: AppCompatButton){
        selectedButton.setTypeface(null, Typeface.BOLD)
        selectedButton.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
        selectedButton.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.green))

        notSelectedButton.setTypeface(null, Typeface.NORMAL)
        notSelectedButton.setTextColor(ContextCompat.getColor(requireContext(), R.color.black))
        notSelectedButton.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.gray))
    }

    private fun showMovieDetailsView(){
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.anim.slide_in, R.anim.fade_out, R.anim.fade_in, R.anim.slide_out).replace(R.id.fragmentContainer, MovieDetailsFragment()).addToBackStack(null).commit()
    }

    private fun showSearchView(){
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentContainer, SearchMoviesFragment()).addToBackStack(null).commit()
    }

}