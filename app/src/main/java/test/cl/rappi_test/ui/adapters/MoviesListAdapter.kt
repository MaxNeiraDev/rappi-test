package test.cl.rappi_test.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import test.cl.rappi_test.R
import test.cl.rappi_test.api.models.Movie
import test.cl.rappi_test.utils.Utility

class MoviesListAdapter (private val context: Context, private var list: MutableList<Movie>, private var listener: OnItemClickListener) : RecyclerView.Adapter<MoviesListAdapter.MyViewHolder>() {

    interface OnItemClickListener { fun onItemClick(item: Movie, position: Int, view: View, holder: MyViewHolder) }

    var lastShowedIndex = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(context)
        val view: View = inflater.inflate(R.layout.movie_row,parent,false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int { return list.size }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = list[position]
        if(Utility.isNetworkAvailable(context)) {
            if (item.posterPath != null && holder.movieImage != null)
                Picasso.get().load("https://image.tmdb.org/t/p/w500/${item.posterPath!!}").resize(100, 100).into(holder.movieImage)
        }
        holder.movieTitle?.text = item.title
        holder.movieRelease?.text = item.releaseDate
        holder.bind(list[position], listener, holder.itemView, holder)
    }

    fun getItem(index: Int): Movie? = if(index != -1 && list.isNotEmpty()) list[index] else null

    fun resetLastShowedIndex() { lastShowedIndex = 0 }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view){

        var movieImage: ImageView? = null
        var movieRelease: TextView? = null
        var movieTitle: TextView? = null

        init {
            movieImage = view.findViewById(R.id.movieImage)
            movieRelease = view.findViewById(R.id.movieRelease)
            movieTitle = view.findViewById(R.id.movieTitle)
        }

        fun bind(item: Movie, listener: OnItemClickListener, view: View, holder: MyViewHolder) {
            itemView.setOnClickListener { listener.onItemClick(item, adapterPosition, view, holder) }
        }

    }
}