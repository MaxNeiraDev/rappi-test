package test.cl.rappi_test.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import test.cl.rappi_test.R
import test.cl.rappi_test.ui.fragments.ListFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        launchFragment()
    }

    private fun launchFragment(){
        val transaction = this.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentContainer, ListFragment()).addToBackStack(null).commit()
    }

}