package test.cl.rappi_test.ui.fragments

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import test.cl.rappi_test.R
import test.cl.rappi_test.api.models.Movie
import test.cl.rappi_test.databinding.FragmentSearchBinding
import test.cl.rappi_test.ui.adapters.MoviesListAdapter
import test.cl.rappi_test.utils.LoadingDialogManager
import test.cl.rappi_test.utils.Utility
import test.cl.rappi_test.viewmodels.*
import test.cl.rappi_test.viewmodels.factories.SearchFragmentViewModelFactory


class SearchMoviesFragment: Fragment(), MoviesListAdapter.OnItemClickListener {

    private var binding: FragmentSearchBinding?= null
    private var myView: View? = null
    private var myContext: Context? = null

    private lateinit var resultsList: MutableList<Movie>
    private lateinit var resultsListAdapter: MoviesListAdapter

    private val searchFragmentViewModel: SearchFragmentViewModel by lazy { ViewModelProvider(this, SearchFragmentViewModelFactory(myContext!!)).get(SearchFragmentViewModel::class.java) }
    private val sharedViewModel: MovieDetailsViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        myContext = container!!.context
        binding = FragmentSearchBinding.inflate(inflater, container, false)
        myView = binding!!.root
        return myView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            lifecycleOwner = viewLifecycleOwner
            myViewModel = searchFragmentViewModel
            fragment = this@SearchMoviesFragment
        }
        setupList()
        subscribeToModel()
        setupViews()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        hideKeyboard()
        binding = null
    }

    override fun onItemClick(item: Movie, position: Int, view: View, holder: MoviesListAdapter.MyViewHolder) {
        if(Utility.isNetworkAvailable(requireActivity())) {
            sharedViewModel.setClickedIndex(position)
            sharedViewModel.setMovie(item)
            hideKeyboard()
            showMovieDetailsView()
        }else{
            searchFragmentViewModel.showMessage("Click not available without connection", requireActivity())
        }
    }

    private fun setupViews(){
        binding!!.searchView.addTextChangedListener {
            it?.let {
                searchFragmentViewModel.inputSearch.postValue(it.toString())
            }
        }
        hideKeyboard()
        binding!!.searchView.setText(searchFragmentViewModel.inputSearch.value!!.toString())
    }

    private fun subscribeToModel(){
        searchFragmentViewModel.inputSearch.observe(requireActivity(), {
            it?.let { searchText ->
                resultsListAdapter.resetLastShowedIndex()
                if (searchText.isNotEmpty()) LoadingDialogManager.showLoader(requireActivity(), "Searching movies")
                searchFragmentViewModel.search(requireActivity())
            }
        })

        binding!!.myViewModel!!.getResultsData().observe(requireActivity(), { t ->
            resultsList.clear()
            t?.let {
                resultsList.addAll(it)
                if (searchFragmentViewModel.searchMoviesRepository.lastRequiredPage.value!! > 1) binding!!.resultsRecyclerView.layoutManager!!.scrollToPosition(resultsListAdapter.lastShowedIndex + 1)
                LoadingDialogManager.hideLoader()
            }
            resultsListAdapter.notifyDataSetChanged()
        })

    }

    private fun setupList(){
        binding!!.resultsRecyclerView.layoutManager = LinearLayoutManager(myContext!!)
        resultsList = mutableListOf()
        searchFragmentViewModel.getResultsData().value?.let { resultsList.addAll(it) }
        resultsListAdapter = MoviesListAdapter(myContext!!, resultsList, this)
        binding!!.resultsRecyclerView.adapter = resultsListAdapter

        binding!!.resultsRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (Utility.isNetworkAvailable(myContext!!)) {
                        searchFragmentViewModel.loadMoreElements(requireActivity())
                        resultsListAdapter.lastShowedIndex = resultsListAdapter.itemCount
                        LoadingDialogManager.showLoader(requireActivity(), "Loading new elements")
                    } else {
                        searchFragmentViewModel.showMessage("Unable to load more items without connection", requireActivity())
                    }
                }
            }
        })
    }

    private fun showMovieDetailsView(){
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.anim.slide_in, R.anim.fade_out, R.anim.fade_in, R.anim.slide_out).replace(R.id.fragmentContainer, MovieDetailsFragment()).addToBackStack(null).commit()
    }

    private fun hideKeyboard() {
        if (activity != null) {
            val inputMethodManager: InputMethodManager = requireActivity().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            if (requireActivity().currentFocus != null) inputMethodManager.hideSoftInputFromWindow(requireActivity().currentFocus!!.windowToken, 0)
        }
    }

}