package test.cl.rappi_test.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.viewpager.widget.PagerAdapter
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import test.cl.rappi_test.R
import test.cl.rappi_test.api.models.MovieVideo
import test.cl.rappi_test.utils.Utility
import java.util.*

internal class MovieVideosViewPagerAdapter(context: Context, private val videos: ArrayList<MovieVideo>) : PagerAdapter() {

    private var mLayoutInflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int { return videos.size }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as ConstraintLayout
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView: View = mLayoutInflater.inflate(R.layout.video_item, container, false)
        val itemYoutubePlayerView = itemView.findViewById<YouTubePlayerView>(R.id.itemYoutubePlayerView)

        itemView.findViewById<AppCompatTextView>(R.id.videoTitle).text = videos[position].name!!

        itemYoutubePlayerView.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
            override fun onReady(youTubePlayer: YouTubePlayer) {
                if(Utility.isNetworkAvailable(itemView.context))
                    youTubePlayer.cueVideo(videos[position].key!!, 0f)
            }
        })

        Objects.requireNonNull(container).addView(itemView)
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as ConstraintLayout)
    }
}
