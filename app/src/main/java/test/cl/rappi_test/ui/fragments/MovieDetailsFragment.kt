package test.cl.rappi_test.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.squareup.picasso.Picasso
import test.cl.rappi_test.api.models.MovieVideo
import test.cl.rappi_test.databinding.FragmentMovieDetailsBinding
import test.cl.rappi_test.ui.adapters.MovieVideosViewPagerAdapter
import test.cl.rappi_test.utils.Utility
import test.cl.rappi_test.viewmodels.MovieDetailsViewModel

class MovieDetailsFragment : Fragment() {

    private var binding: FragmentMovieDetailsBinding ?= null
    private var myView: View? = null
    private var myContext: Context? = null
    private val movieDetailsViewModel: MovieDetailsViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        myContext = container!!.context
        binding = FragmentMovieDetailsBinding.inflate(inflater, container, false)
        myView = binding!!.root
        return myView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            lifecycleOwner = viewLifecycleOwner
            viewModel = movieDetailsViewModel
            fragment = this@MovieDetailsFragment
        }

        movieDetailsViewModel.getVideos(myContext!!, movieDetailsViewModel.getMovie().value!!.id.toString())

        if(Utility.isNetworkAvailable(myContext!!)) Picasso.get().load("https://image.tmdb.org/t/p/w500/${movieDetailsViewModel.getMovie().value!!.posterPath}").resize(100,100).into(binding!!.movieImage)

        movieDetailsViewModel.getMovieVideos().observe(requireActivity(), { t ->
            t?.let {
                    videos ->
                        if (videos.isNotEmpty()) {
                            setupVideosViewPager(ArrayList(videos))
                            movieDetailsViewModel.getMovie().value!!.videosData = ArrayList(videos)
                        }else{
                            binding!!.videosContainer.visibility = View.GONE
                        }
            }
        })
    }

    private fun setupVideosViewPager(keys: ArrayList<MovieVideo>){
        val mViewPagerAdapter = MovieVideosViewPagerAdapter(myContext!!, keys)
        binding!!.videosViewPager.adapter = mViewPagerAdapter
        binding!!.videosViewPagerIndicator.setupWithViewPager(binding!!.videosViewPager, true)
        binding!!.videosContainer.visibility = View.VISIBLE
    }

}