package test.cl.rappi_test.utils

import android.app.AlertDialog
import android.content.Context
import dmax.dialog.SpotsDialog

object LoadingDialogManager {

    private var loader: AlertDialog? = null

    private fun setupLoader(context: Context){
        loader = SpotsDialog.Builder().setContext(context).build()
        loader?.setCancelable(false)
    }

    fun showLoader(context: Context, message: String){
        if(loader == null) setupLoader(context)
        loader!!.setMessage(message)
        if(isShowing()) dismissLoader()
        loader!!.show()
    }

    fun hideLoader() { dismissLoader() }

    private fun dismissLoader() { if (loader != null && loader!!.isShowing) loader!!.dismiss() }

    private fun isShowing(): Boolean { return loader != null && loader!!.isShowing }

}