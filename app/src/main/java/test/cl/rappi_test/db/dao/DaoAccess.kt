package test.cl.rappi_test.db.dao

import androidx.room.*
import kotlinx.coroutines.flow.Flow
import test.cl.rappi_test.db.models.MoviesTableModel

@Dao
interface DaoAccess {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovie(movie: MoviesTableModel)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovies(movies: List<MoviesTableModel>)

    @Query("SELECT * FROM Movies")
    fun getAllMovies(): Flow<List<MoviesTableModel>>

    @Delete
    fun deleteMovie(car: MoviesTableModel): Int

    @Query("DELETE FROM Movies")
    suspend fun deleteAllMovies(): Int

}