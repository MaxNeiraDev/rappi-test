package test.cl.rappi_test.db.repositories

import android.content.Context
import androidx.lifecycle.asLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import test.cl.rappi_test.db.database.MultimediasDatabase
import test.cl.rappi_test.db.models.MoviesTableModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class MultimediasDBRepository {

    companion object {

        var multimedias: Flow<List<MoviesTableModel>> ?= null
        private var database: MultimediasDatabase ?= null

        private fun initializeDB(context: Context) {
            database = MultimediasDatabase.getDatabaseClient(context)
        }

        fun getMultimedias() = multimedias!!.asLiveData().value

        fun insertMovie(context: Context, newMovie: MoviesTableModel) {
            if (database == null) initializeDB(context)
            CoroutineScope(Dispatchers.IO).launch {
                database!!.geDao().insertMovie(newMovie)
            }
        }

        fun insertMovies(context: Context, movies: List<MoviesTableModel>) {
            if (database == null) initializeDB(context)
            CoroutineScope(Dispatchers.IO).launch {
                database!!.geDao().insertMovies(movies)
            }
        }

        fun getAllMovies(context: Context): Flow<List<MoviesTableModel>>? {
            if (database == null) initializeDB(context)
            multimedias = database!!.geDao().getAllMovies()
            return multimedias
        }

        fun deleteMovie(context: Context, elementToDelete: MoviesTableModel) = runBlocking{
            var index = -1
            val job = CoroutineScope(Dispatchers.IO).launch {
                if (database == null) initializeDB(context)
                index = database!!.geDao().deleteMovie(elementToDelete)
            }
            job.join()
            return@runBlocking index
        }

        fun deleteAllMovies(context: Context){
            if (database == null) initializeDB(context)
            database?.let {
                CoroutineScope(Dispatchers.IO).launch {
                    database!!.geDao().deleteAllMovies()
                }
            }
        }

    }

}