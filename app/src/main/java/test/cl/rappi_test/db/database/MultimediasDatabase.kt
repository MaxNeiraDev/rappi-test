package test.cl.rappi_test.db.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import kotlinx.coroutines.CoroutineScope
import test.cl.rappi_test.db.dao.DaoAccess
import test.cl.rappi_test.db.models.MoviesTableModel

@Database(entities = [MoviesTableModel::class], version = 4, exportSchema = false)
abstract class MultimediasDatabase: RoomDatabase() {

    abstract fun geDao(): DaoAccess

    companion object {

        @Volatile
        private var INSTANCE: MultimediasDatabase ?= null

        fun getDatabaseClient(context: Context, scope: CoroutineScope ?= null): MultimediasDatabase{
            if(INSTANCE != null) return INSTANCE!!
            synchronized(this){
                INSTANCE = Room
                    .databaseBuilder(context, MultimediasDatabase::class.java, "multimedias_db")
                    .fallbackToDestructiveMigration()
                    .build()
                return INSTANCE!!
            }
        }

    }

}