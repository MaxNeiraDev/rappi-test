package test.cl.rappi_test.viewmodels

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import test.cl.rappi_test.api.models.Movie
import test.cl.rappi_test.api.repositories.SearchMoviesRepository
import test.cl.rappi_test.utils.Utility

class SearchFragmentViewModel(context: Context): ViewModel() {

    private var resultsList = MutableLiveData<List<Movie>>()
    val searchMoviesRepository: SearchMoviesRepository by lazy { SearchMoviesRepository }
    val inputSearch = MutableLiveData<String>("")

    init {
        resultsList = searchMoviesRepository.getMutableLiveData()
    }

    fun getResultsData(): MutableLiveData<List<Movie>> =  resultsList

    fun loadMoreElements(context: Context){
        if(Utility.isNetworkAvailable(context)) {
            if(!searchMoviesRepository.dataFullLoaded()) {
                getDataFromRepository()
            }else
                showMessage("All movies loaded for your search", context)
        }
    }

    fun search(context: Context){
        if(Utility.isNetworkAvailable(context)) {
            searchMoviesRepository.resetValues()
            getDataFromRepository()
        }else{
            showMessage("Connection error. Unable to search movies without connection. Please try again later", context)
        }
    }

    fun showMessage(message: String, context: Context) = Toast.makeText(context, message, Toast.LENGTH_LONG).show()

    private fun getDataFromRepository(){
        viewModelScope.launch {
            resultsList = searchMoviesRepository.searchMoviesRequest(inputSearch.value!!)
            resultsList.value = searchMoviesRepository.getResults()
        }
    }

}