package test.cl.rappi_test.viewmodels

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.*
import kotlinx.coroutines.launch
import test.cl.rappi_test.api.models.Movie
import test.cl.rappi_test.api.repositories.MoviesRepository
import test.cl.rappi_test.db.models.MoviesTableModel
import test.cl.rappi_test.db.repositories.MultimediasDBRepository
import test.cl.rappi_test.utils.Utility

class ListFragmentViewModel(context: Context): ViewModel() {

    private var moviesList = MutableLiveData<List<Movie>>()
    private val dataFromDB = MutableLiveData<Boolean>()
    private val dataFromWS = MutableLiveData<Boolean>()
    val moviesRepository: MoviesRepository by lazy { MoviesRepository }

    var topRatedTypeSelected = MutableLiveData<Boolean>(true)
    var changeMultimediaTypeAction = MutableLiveData<Boolean>(false)

    var dbElementsList :LiveData<List<MoviesTableModel>> ?= MultimediasDBRepository.getAllMovies(context)!!.asLiveData()

    init {
        dataFromDB.postValue(false)
        dataFromWS.postValue(true)
        getMovies(context)
    }

    fun getMoviesData(): MutableLiveData<List<Movie>> =  moviesList

    fun getDataFromDBState(): Boolean = dataFromDB.value!!

    fun getDataFromWSState(): Boolean = dataFromWS.value!!

    fun getDBElements(): LiveData<List<MoviesTableModel>> ?= dbElementsList

    fun getMovies(context: Context){
        if(Utility.isNetworkAvailable(context)) {
            dataFromWS.postValue(true)
            dataFromDB.postValue(false)
            if(!moviesRepository.dataFullLoaded()) {
                if(moviesRepository.totalResults.value ==0) deleteAllSavedElements(context)
                getDataFromRepository()
            }else
                showMessage("All movies loaded", context)
        }else {
            dataFromWS.postValue(false)
            dataFromDB.postValue(true)
            getSavedElements(context)
        }
    }

    fun showMessage(message: String, context: Context) = Toast.makeText(context, message, Toast.LENGTH_LONG).show()

    fun saveCurrentElements(context: Context){
        moviesList.value?.let {
            val convertedElements = convertElementsToBdModel(it)
            if(convertedElements.isNotEmpty()){
                MultimediasDBRepository.insertMovies(context, convertedElements)
            }
        }
    }

    private fun deleteAllSavedElements(context: Context){
        MultimediasDBRepository.deleteAllMovies(context)
        dbElementsList = MultimediasDBRepository.multimedias!!.asLiveData()
    }

    fun useDataFromDB(inputData: List<MoviesTableModel>){
        val data = convertElementsToApiModel(inputData)
        moviesList.postValue(data)
    }

    private fun getSavedElements(context: Context){
        val queryResult = MultimediasDBRepository.multimedias
        queryResult?.let { flowList ->
            dbElementsList = flowList.asLiveData()
        }
    }

    private fun convertElementsToApiModel(elements: List<MoviesTableModel>): ArrayList<Movie>{
        val convertedElements = ArrayList<Movie> ()
        elements.map {
            val convertedElement = Movie(
                it.id, it.title, it.posterPath, it.overview,
                it.voteAverage, it.voteCount, it.popularity, it.originalLanguage,
                it.releaseDate
            )
            convertedElements.add(convertedElement)
        }
        return convertedElements
    }

    private fun convertElementsToBdModel(elements: List<Movie>): ArrayList<MoviesTableModel>{
        val convertedElements = ArrayList<MoviesTableModel> ()
        elements.map {
            val convertedElement = MoviesTableModel(
                it.id!!, it.title!!, it.posterPath!!, it.overview!!,
                it.voteAverage!!, it.voteCount!!,  it.popularity!!, it.originalLanguage!!,
                it.releaseDate!!
            )
            convertedElements.add(convertedElement)
        }
        return convertedElements
    }

    fun multimediasTypeChange(context: Context){
        deleteAllSavedElements(context)
        moviesRepository.resetValues()
        getDataFromRepository()
        changeMultimediaTypeAction.value = false
    }

    private fun getDataFromRepository(){
        if(topRatedTypeSelected.value!!){
            viewModelScope.launch {
                moviesList = moviesRepository.getTopRatedMutableLiveData()
                moviesList.value = moviesRepository.getMovies()
            }
        }else{
            viewModelScope.launch {
                moviesList = moviesRepository.getPopularMutableLiveData()
                moviesList.value = moviesRepository.getMovies()
            }
        }
    }

}