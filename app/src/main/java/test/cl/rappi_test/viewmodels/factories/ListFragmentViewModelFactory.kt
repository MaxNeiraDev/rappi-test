package test.cl.rappi_test.viewmodels.factories

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import test.cl.rappi_test.viewmodels.ListFragmentViewModel

class ListFragmentViewModelFactory(private val context: Context): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ListFragmentViewModel(context) as T
    }

}