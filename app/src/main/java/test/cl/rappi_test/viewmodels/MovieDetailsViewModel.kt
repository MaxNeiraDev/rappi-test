package test.cl.rappi_test.viewmodels

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import test.cl.rappi_test.api.models.Movie
import test.cl.rappi_test.api.models.MovieVideo
import test.cl.rappi_test.api.repositories.MovieVideosRepository
import test.cl.rappi_test.utils.Utility

class MovieDetailsViewModel: ViewModel() {

    val selectedMovie = MutableLiveData<Movie>()
    private val clickedIndex = MutableLiveData<Int>()
    private var videosList = MutableLiveData<List<MovieVideo>>()
    val movieVideosRepository: MovieVideosRepository by lazy { MovieVideosRepository }

    fun getMovie():MutableLiveData<Movie> = selectedMovie

    fun setMovie(movie: Movie){ selectedMovie.value = movie }

    fun getMovieVideos():MutableLiveData<List<MovieVideo>> = videosList

    fun setClickedIndex(value: Int) = clickedIndex.postValue(value)

    fun getVideos(context: Context, movieId: String){
        if(Utility.isNetworkAvailable(context)) {
            viewModelScope.launch {
                videosList = movieVideosRepository.getMutableLiveData(movieId)
                videosList.value = movieVideosRepository.getMovieVideos()
            }
        }
    }

}