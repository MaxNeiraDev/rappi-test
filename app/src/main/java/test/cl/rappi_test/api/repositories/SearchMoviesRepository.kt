package test.cl.rappi_test.api.repositories

import android.util.Log
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import test.cl.rappi_test.api.client.ApiClient
import test.cl.rappi_test.api.models.Movie
import test.cl.rappi_test.api.models.MoviesResponse

object SearchMoviesRepository : Callback<MoviesResponse> {

    var moviesMutableLiveData = MutableLiveData<List<Movie>>()
    val totalResults: MutableLiveData<Int> = MutableLiveData(0)
    val totalPages: MutableLiveData<Int> = MutableLiveData(0)
    val lastRequiredPage: MutableLiveData<Int> = MutableLiveData(0)
    val currentElementsCounter: MutableLiveData<Int> = MutableLiveData(0)
    private val movies: ArrayList<Movie> = ArrayList()

    fun searchMoviesRequest(query: String): MutableLiveData<List<Movie>> {
        ApiClient.apiService.searchMovies(ApiClient.getKey(), query,lastRequiredPage.value!! + 1).enqueue(this)
        return moviesMutableLiveData
    }

    fun getMutableLiveData() = moviesMutableLiveData

    fun getResults() = moviesMutableLiveData.value

    fun getCurrentMoviesCounter() = currentElementsCounter.value

    fun dataFullLoaded(): Boolean { return (lastRequiredPage.value == totalPages.value && lastRequiredPage.value != 0 && lastRequiredPage.value != 0) }

    override fun onFailure(call: Call<MoviesResponse>, t: Throwable) {
        Log.e("error", t.message.toString())
    }

    override fun onResponse(call: Call<MoviesResponse>, response: Response<MoviesResponse>) {
        val userResponse = response.body()
        userResponse?.let { it ->
            if(it.page!! == 1) movies.clear()
            movies.addAll(orderMovies(it.results as ArrayList<Movie>))
            moviesMutableLiveData.postValue(movies)
            lastRequiredPage.value = it.page!!
            if(totalResults.value == 0) totalResults.value = it.totalResults!!
            if(totalPages.value == 0) totalPages.value = it.totalPages!!
            currentElementsCounter.value = it.results.size + currentElementsCounter.value!!
        }
    }

    private fun orderMovies(originalList: ArrayList<Movie>): List<Movie>{
        return originalList.sortedByDescending { it.voteAverage }
    }

    fun resetValues(){
        totalResults.value = 0
        totalPages.value = 0
        lastRequiredPage.value = 0
        currentElementsCounter.value = 0
        movies.clear()
    }

}