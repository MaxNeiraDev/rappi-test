package test.cl.rappi_test.api.client

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {

    private const val BASE_URL: String = "https://api.themoviedb.org/3/"
    private const val API_KEY: String = "855c7cfec003eeb0a29204dc10c1c379"

    private val GSON : Gson by lazy { GsonBuilder().setLenient().create() }

    private val httpClient : OkHttpClient by lazy { OkHttpClient.Builder().build() }

    private val retrofit : Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create(GSON))
            .build()
    }

    val apiService : ApiService by lazy{
        retrofit.create(ApiService::class.java)
    }

    fun getKey() = API_KEY

}