package test.cl.rappi_test.api.repositories

import android.util.Log
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import test.cl.rappi_test.api.client.ApiClient
import test.cl.rappi_test.api.models.Movie
import test.cl.rappi_test.api.models.MoviesResponse

object MoviesRepository : Callback<MoviesResponse> {

    var moviesMutableLiveData = MutableLiveData<List<Movie>>()
    val totalResults: MutableLiveData<Int> = MutableLiveData(0)
    val totalPages: MutableLiveData<Int> = MutableLiveData(0)
    val lastRequiredPage: MutableLiveData<Int> = MutableLiveData(0)
    val currentElementsCounter: MutableLiveData<Int> = MutableLiveData(0)
    private val movies: ArrayList<Movie> = ArrayList()
    private var topRatedSelected: Boolean = true

    fun getTopRatedMutableLiveData(): MutableLiveData<List<Movie>> {
        ApiClient.apiService.getTopRatedMovies(ApiClient.getKey(), lastRequiredPage.value!! + 1).enqueue(this)
        topRatedSelected = true
        return moviesMutableLiveData
    }

    fun getPopularMutableLiveData(): MutableLiveData<List<Movie>> {
        ApiClient.apiService.getPopularMovies(ApiClient.getKey(), lastRequiredPage.value!! + 1).enqueue(this)
        topRatedSelected = false
        return moviesMutableLiveData
    }

    fun getMovies() = moviesMutableLiveData.value


    fun getCurrentMoviesCounter() = currentElementsCounter.value

    fun dataFullLoaded(): Boolean { return (lastRequiredPage.value == totalPages.value && lastRequiredPage.value != 0 && lastRequiredPage.value != 0) }

    override fun onFailure(call: Call<MoviesResponse>, t: Throwable) {
        Log.e("error", t.message.toString())
    }

    override fun onResponse(call: Call<MoviesResponse>, response: Response<MoviesResponse>) {
        val userResponse = response.body()
        userResponse?.let { it ->
            movies.addAll(orderMovies(it.results as ArrayList<Movie>))
            moviesMutableLiveData.postValue(movies)
            lastRequiredPage.value = it.page!!
            if(totalResults.value == 0) totalResults.value = it.totalResults!!
            if(totalPages.value == 0) totalPages.value = it.totalPages!!
            currentElementsCounter.value = it.results.size + currentElementsCounter.value!!
        }
    }

    private fun orderMovies(originalList: ArrayList<Movie>): List<Movie>{
        if(topRatedSelected){
            originalList.sortedWith(compareBy<Movie> { it.voteAverage }.thenBy { it.voteCount })
            originalList.reversed()
        }else{
            originalList.sortedByDescending { it.popularity }
        }
        return  originalList
    }

    fun resetValues(){
        totalResults.value = 0
        totalPages.value = 0
        lastRequiredPage.value = 0
        currentElementsCounter.value = 0
        movies.clear()
    }

}