package test.cl.rappi_test.api.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Movie(

    @SerializedName("id")
    val id: Int? = null,

    @SerializedName("title")
    val title: String? = null,

    @SerializedName("poster_path")
    var posterPath: String? = null,

    @SerializedName("overview")
    var overview: String? = null,

    @SerializedName("vote_average")
    var voteAverage: String? = null,

    @SerializedName("vote_count")
    var voteCount: String? = null,

    @SerializedName("popularity")
    var popularity: Double? = null,

    @SerializedName("original_language")
    var originalLanguage: String? = null,

    @SerializedName("release_date")
    var releaseDate: String? = null,

    var videosData: ArrayList<MovieVideo> = ArrayList()

) : Serializable