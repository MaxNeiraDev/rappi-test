package test.cl.rappi_test.api.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class MoviesResponse (

    @SerializedName("results")
    val results: List<Movie>  ?= null,

    @SerializedName("page")
    val page: Int ? = null,

    @SerializedName("total_pages")
    val totalPages: Int ? = null,

    @SerializedName("total_results")
    val totalResults: Int ? = null

): Serializable