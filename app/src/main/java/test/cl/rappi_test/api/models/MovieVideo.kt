package test.cl.rappi_test.api.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class MovieVideo(

    @SerializedName("id")
    val id: String? = null,

    @SerializedName("key")
    val key: String? = null,

    @SerializedName("name")
    var name: String? = null,

    @SerializedName("site")
    var site: String? = null,

    @SerializedName("size")
    var size: Int? = null,

    @SerializedName("type")
    var type: String? = null

) : Serializable