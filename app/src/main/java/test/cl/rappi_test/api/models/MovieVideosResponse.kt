package test.cl.rappi_test.api.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class MovieVideosResponse (

    @SerializedName("results")
    val results: List<MovieVideo>  ?= null,

    @SerializedName("id")
    val id: Int ? = null

): Serializable