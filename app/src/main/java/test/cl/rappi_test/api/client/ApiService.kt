package test.cl.rappi_test.api.client

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query
import test.cl.rappi_test.api.models.MovieVideosResponse
import test.cl.rappi_test.api.models.MoviesResponse

interface ApiService {

    @Headers("Content-Type:application/json; charset=UTF-8")
    @GET("movie/top_rated?")
    fun getTopRatedMovies(@Query("api_key") apiKey: String, @Query("page") page: Int, @Query("language") language: String = "en-US"): Call<MoviesResponse>

    @Headers("Content-Type:application/json; charset=UTF-8")
    @GET("movie/{id}/videos?")
    fun getMovieVideos(@Path("id") id: String, @Query("api_key") apiKey: String, @Query("language") language: String = "en-US"): Call<MovieVideosResponse>

    @Headers("Content-Type:application/json; charset=UTF-8")
    @GET("movie/popular?")
    fun getPopularMovies(@Query("api_key") apiKey: String, @Query("page") page: Int, @Query("language") language: String = "en-US"): Call<MoviesResponse>

    @Headers("Content-Type:application/json; charset=UTF-8")
    @GET("search/movie?")
    fun searchMovies(@Query("api_key") apiKey: String, @Query("query") query: String, @Query("page") page: Int, @Query("language") language: String = "en-US"): Call<MoviesResponse>

}