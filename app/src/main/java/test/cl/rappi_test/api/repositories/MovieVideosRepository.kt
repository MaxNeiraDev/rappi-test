package test.cl.rappi_test.api.repositories

import android.util.Log
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import test.cl.rappi_test.api.client.ApiClient
import test.cl.rappi_test.api.models.MovieVideo
import test.cl.rappi_test.api.models.MovieVideosResponse

object MovieVideosRepository : Callback<MovieVideosResponse> {

    private var movieVideosMutableLiveData = MutableLiveData<List<MovieVideo>>()
    private val videos: ArrayList<MovieVideo> = ArrayList()

    fun getMutableLiveData(movieId: String): MutableLiveData<List<MovieVideo>> {
        ApiClient.apiService.getMovieVideos(movieId, ApiClient.getKey()).enqueue(this)
        return movieVideosMutableLiveData
    }

    fun getMovieVideos() = movieVideosMutableLiveData.value

    override fun onFailure(call: Call<MovieVideosResponse>, t: Throwable) {
        Log.e("error", t.message.toString())
    }

    override fun onResponse(call: Call<MovieVideosResponse>, response: Response<MovieVideosResponse>) {
        val userResponse = response.body()
        userResponse?.let { it ->
            videos.clear()
            it.results?.let { resultList ->
                resultList.map { element ->
                    if(element.site!!.toLowerCase() == "youtube") videos.add(element)
                }
            }
            movieVideosMutableLiveData.postValue(videos)
        }
    }

}