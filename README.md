Desarrollo de prueba técnica
=============================================

### Consideraciones de la solución

- Trabaje exclusivamente con las películas excluyendo series para ahorrar tiempo y conseguir más desarrollo de funcionalidades
- Existe una animación al momento de desplegar la Vista de detalle (MovieDetailsFragment) y al momento de salir de la misma
- No utilicé inyección de dependencias
- No desarrollé pruebas unitarias
- En la Vista de detalle (MovieDetailsFragment) solamente desplegué videos provenientes desde en Youtube, también en función de priorizar desarrollo de funcionalidades

#### Principales librerías utilizadas 

- Lifecycle: (ViewModel + LiveData): utilizada para el manejo de información e implementación del patrón MVVM
- Room (Flow): utilizada para el acceso y manejo de datos en la base de datos local
- Retrofit (Gson): utilizada para realizar las solicitudes de información a la api y la transformación desde el json recibido a los modelos de datos de los objetos requeridos
- Picasso: utilizada para la carga dinámica de imágenes desde una url a la ImageView
- Dialog: utilizada para el despliegue de un cuadro de diálogo de carga
- Youtube Player: utilizada para el despliegue y reproducción de videos de Youtube
- Androidx Design Elements (Recyclerview + CardView): utilizada para el despliegue de las distintas listas (Recyclerview) y sus elementos (CardView) en la app
